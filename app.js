window.onbeforeunload = function () {
    if (doubleClickTimer.times.length > 0) {
        window.sessionStorage.setItem('countedDoubleClickTimes', doubleClickTimer.times);
    }
};

function DoubleClickTimer() {
    var doubleClickTimer = this;
    doubleClickTimer.count = 0;
    doubleClickTimer.times = [];
    doubleClickTimer.previousDate;
    doubleClickTimer.ping = function () {

        doubleClickTimer.count++;
        if (doubleClickTimer.previousDate !== undefined) {

            let currentDate = new Date();
            let timeBetweenTwoClicks = currentDate - doubleClickTimer.previousDate;

            doubleClickTimer.times.push(timeBetweenTwoClicks);
            doubleClickTimer.times.sort(function (a, b) {
                return a - b;
            });
            doubleClickTimer.times.splice(5);
            doubleClickTimer.previousDate = currentDate;
        }
        else {
            doubleClickTimer.previousDate = new Date();
            if (window.sessionStorage.getItem('countedDoubleClickTimes')) {
                doubleClickTimer.times = window.sessionStorage.getItem('countedDoubleClickTimes').split(',');
            }
        }
    }
    doubleClickTimer.clearBestTimesButton = function () {
        doubleClickTimer.times = [];
        if (window.sessionStorage.getItem('countedDoubleClickTimes')) {
            sessionStorage.removeItem("countedDoubleClickTimes");
        }
    }
}

var doubleClickTimer = new DoubleClickTimer()

var Page = {
    view: function (vnode) {
        return m('div', [
            m("nav.navbar.navbar-default",
                m(".container-fluid",
                    [
                        m(".navbar-header",
                            m("a.navbar-brand[href='#']",
                                "Double Click Timer"
                            )
                        ),
                        m("ul.nav.navbar-nav",
                            [
                                m("li.active",
                                    m("a[href='#']",
                                        "Home"
                                    )
                                ),
                                m("li",
                                    m("a[href='#']",
                                        "Page 1"
                                    )
                                ),
                                m("li",
                                    m("a[href='#']",
                                        "Page 2"
                                    )
                                ),
                                m("li",
                                    m("a[href='#']",
                                        "Page 3"
                                    )
                                )
                            ]
                        )
                    ]
                )
            ),
            m('div', { class: 'row' }, [
                m('div', { class: 'col-xs-6 col-xs-offset-3' }, [
                    m('h4', { class: 'text-center game-headline' }, 'Measuring how fast user can take double clicks. Top five fastes double clicks are listed bellow.'),
                    m('button', {
                        class: "btn btn-default",
                        onclick: doubleClickTimer.ping,
                    }, 'click me!'),
                    m('button', {
                        class: "btn btn-danger",
                        onclick: doubleClickTimer.clearBestTimesButton,
                    }, 'clear best times!'),
                    m('p', 'You have clicked ', doubleClickTimer.count, ' times. Try one more.'),
                    m('ol', { class: 'list-group' }, [
                        'Double click difference:',
                        doubleClickTimer.times.map(function (t) {
                            return m('li', { class: "timestamp-value list-group-item" }, t.toString() + ' miliseconds');
                        }),
                    ]),
                ])
            ]),


        ])
    },
}

m.mount(document.body, Page)
